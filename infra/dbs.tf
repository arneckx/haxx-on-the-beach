resource "aws_dynamodb_table" "locations_dynamodb_table" {
  name         = "plane_locations_events"
  billing_mode = "PAY_PER_REQUEST"

  hash_key  = "id"
  range_key = "timestamp"

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "timestamp"
    type = "S"
  }

  attribute {
    name = "plane_id"
    type = "S"
  }

  global_secondary_index {
    name            = "by_plane_id"
    hash_key        = "plane_id"
    projection_type = "ALL"
    range_key       = "timestamp"
  }
}
