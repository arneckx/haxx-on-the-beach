resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "create_location" {
  filename      = var.create_location_build_path
  function_name = "create_location"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "create.apply"

  source_code_hash = filebase64sha256(var.create_location_build_path)

  runtime = "python3.8"
}

resource "aws_lambda_function" "find_locations" {
  filename      = var.find_location_build_path
  function_name = "find_locations"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "find.by"

  source_code_hash = filebase64sha256(var.find_location_build_path)

  runtime = "python3.8"
}
