provider "aws" {
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true

  endpoints {
    apigateway       = "http://localhost:4566"
    cloudformation   = "http://localhost:4566"
    cloudwatch       = "http://localhost:4566"
    cloudwatchlogs   = "http://localhost:4566"
    cloudwatchevents = "http://localhost:4566"
    dynamodb         = "http://localhost:4566"
    ec2              = "http://localhost:4566"
    iam              = "http://localhost:4566"
    lambda           = "http://localhost:4566"
  }

  default_tags {
    tags = {
      Owner       = "arneckx"
      Project     = "haxx-on-the-beach"
      Team        = "java"
      Terraform   = "haxx-on-the-beach/infra"
      Environment = var.env
    }
  }
}
