resource "aws_api_gateway_rest_api" "haxx_on_the_beach_api" {
  name = "haxx_on_the_beach_api"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_deployment" "this" {
  rest_api_id = aws_api_gateway_rest_api.haxx_on_the_beach_api.id

  triggers = {
    redeployment = sha1(join(",", concat(
      [
        jsonencode(aws_api_gateway_resource.planes),
        jsonencode(aws_api_gateway_resource.planes_plane_id),
        jsonencode(aws_api_gateway_resource.planes_plane_id_locations),
        jsonencode(aws_api_gateway_resource.health_resource)
      ]
    )))
  }
}

resource "aws_api_gateway_method" "health" {
  rest_api_id   = aws_api_gateway_rest_api.haxx_on_the_beach_api.id
  resource_id   = aws_api_gateway_resource.health_resource.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_method" "get_locations" {
  rest_api_id   = aws_api_gateway_rest_api.haxx_on_the_beach_api.id
  resource_id   = aws_api_gateway_resource.planes_plane_id_locations.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_method" "post_locations" {
  rest_api_id   = aws_api_gateway_rest_api.haxx_on_the_beach_api.id
  resource_id   = aws_api_gateway_resource.planes_plane_id_locations.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "health" {
  rest_api_id             = aws_api_gateway_rest_api.haxx_on_the_beach_api.id
  resource_id             = aws_api_gateway_resource.health_resource.id
  http_method             = "GET"
  integration_http_method = "GET"
  type                    = "HTTP_PROXY"
  uri                     = "http://localhost:4566/health"
}

resource "aws_api_gateway_integration" "create_location" {
  rest_api_id             = aws_api_gateway_rest_api.haxx_on_the_beach_api.id
  resource_id             = aws_api_gateway_resource.planes_plane_id_locations.id
  http_method             = aws_api_gateway_method.post_locations.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.create_location.invoke_arn
}

resource "aws_api_gateway_integration" "find_last_location" {
  rest_api_id             = aws_api_gateway_rest_api.haxx_on_the_beach_api.id
  resource_id             = aws_api_gateway_resource.planes_plane_id_locations.id
  http_method             = aws_api_gateway_method.get_locations.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.find_locations.invoke_arn
}

resource "aws_api_gateway_stage" "api" {
  deployment_id = aws_api_gateway_deployment.this.id
  rest_api_id   = aws_api_gateway_rest_api.haxx_on_the_beach_api.id
  stage_name    = "api"
}

resource "aws_api_gateway_resource" "health_resource" {
  rest_api_id = aws_api_gateway_rest_api.haxx_on_the_beach_api.id
  parent_id   = aws_api_gateway_rest_api.haxx_on_the_beach_api.root_resource_id
  path_part   = "health"
}

resource "aws_api_gateway_resource" "planes" {
  rest_api_id = aws_api_gateway_rest_api.haxx_on_the_beach_api.id
  parent_id   = aws_api_gateway_rest_api.haxx_on_the_beach_api.root_resource_id
  path_part   = "planes"
}

resource "aws_api_gateway_resource" "planes_plane_id" {
  rest_api_id = aws_api_gateway_rest_api.haxx_on_the_beach_api.id
  parent_id   = aws_api_gateway_resource.planes.id
  path_part   = "{plane-id}"
}

resource "aws_api_gateway_resource" "planes_plane_id_locations" {
  rest_api_id = aws_api_gateway_rest_api.haxx_on_the_beach_api.id
  parent_id   = aws_api_gateway_resource.planes_plane_id.id
  path_part   = "locations"
}

resource "aws_iam_role_policy_attachment" "apigateway_cloudwatch" {
  role       = aws_iam_role.iam_for_api_gateway.id
  policy_arn = data.aws_iam_policy.api_gateway_cloudwatch.arn
}

resource "aws_api_gateway_account" "events_apigateway" {
  cloudwatch_role_arn = aws_iam_role.iam_for_api_gateway.arn
}

resource "aws_cloudwatch_log_group" "this" {
  name = "API-Gateway-Execution-Logs_${aws_api_gateway_rest_api.haxx_on_the_beach_api.id}/${aws_api_gateway_stage.api.stage_name}"
}

resource "aws_iam_role" "iam_for_api_gateway" {
  name = "iam_for_api_gateway"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

data "aws_iam_policy" "api_gateway_cloudwatch" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs"
}
