variable "env" {
  type = string
}
variable "create_location_build_path" {
  type        = string
  description = "Path of zipped code for create function."
}
variable "find_location_build_path" {
  type        = string
  description = "Path of zipped code for find function."
}
variable "frontend_build_location" {
  type        = string
  description = "Path of frontend build."
}
