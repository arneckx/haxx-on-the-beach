# Haxx on the beach 2021

[Watch my presentation](https://www.youtube.com/watch?v=n0bCZRLqCZI) or [Read the article](https://www.axxes.com/insights/aws-maar-dan-lokaal)

## Localstack demo project

How to setup localstack and run some basic aws services like api gateway, lambda, sqs, dynamodb

## Requirements

- docker
- npm

## Config
~/.aws/credentials
```
[localstack]
aws_access_key_id = mock_access_key  
aws_secret_access_key = mock_secret_key
region = eu-west-1
```
```
export AWS_PROFILE=localstack
export AWS_REGION=eu-west-1
```

## Start demo

### Start localstack

```
cd ./localstack  
docker compose up &
```  

### Provision infra

```
cd ../src/locations  
./build.sh
cd ../../infra  
terraform apply -auto-approve  
```

use api gateway rest api id to set HAXX_ON_THE_BEACH_API_ID in frontend .env file.

### Inject location

```
curl --location --request POST 'http://localhost:4566/restapis/{correct-rest-api-id}/api/_user_request_/planes/haxx-on-the-beach-plane/locations' \
--header 'Content-Type: application/json' \
--data-raw '{
    "latitude": 51.22900243344351,
    "longitude": 4.4115255616346145
}'
```

### Serve frontend

```
cd ../plane-tracker  
npm run build  
npm install serve
serve -s build
```  

## Stop demo

```
cd ../../infra   
terraform destroy -auto-approve   
cd ../localstack  
docker compose down   
cd ../
```
