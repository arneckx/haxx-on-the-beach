#!/usr/bin/env bash

mkdir ./build

rm ./build/create.zip
rm ./build/find.zip

zip -r ./build/create.zip ./create.py ./build
zip -r ./build/find.zip ./find.py ./build
