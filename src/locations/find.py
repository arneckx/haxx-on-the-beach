import boto3
from boto3.dynamodb.conditions import Key


def by(event, context):
    plane_id = event['path'].split("/")[2]
    print(f'## PLANE -> {plane_id}')
    limit = event['queryStringParameters']['limit']
    print(f'## LIMIT -> {limit}')

    dynamodb = boto3.resource('dynamodb', endpoint_url="http://localhost:4566")

    table = dynamodb.Table('plane_locations_events')
    db_response = table.query(
        IndexName='by_plane_id',
        Limit=int(limit),
        KeyConditionExpression=Key('plane_id').eq(plane_id),
        ScanIndexForward=False,
    )

    if len(db_response["Items"]) > 0:
        item = db_response["Items"][0]
        print(f'## ITEM -> {item}')
        response = {
            "statusCode": 200,
            "body": {"latitude": item["latitude"], "longitude": item["longitude"]},
            "headers": {'content-type': 'application/json'}
        }
    else:
        print("no locations found")
        response = {
            "statusCode": 404,
            "body": {"message": "No locations found."},
            "headers": {'content-type': 'application/json'}
        }

    return response
