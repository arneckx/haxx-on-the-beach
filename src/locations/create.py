from decimal import Decimal
import json
import boto3
from datetime import datetime
import uuid


def apply(event, context):
    plane_id = event['path'].split("/")[2]
    print(f'## PLANE -> {plane_id}')
    location = json.loads(event['body'], parse_float=Decimal)
    print(f'## LOCATION {location}')

    dynamodb = boto3.resource('dynamodb', endpoint_url="http://localhost:4566")

    table = dynamodb.Table('plane_locations_events')
    event_id = str(uuid.uuid4())
    table.put_item(
        Item={
            'id': event_id,
            'plane_id': plane_id,
            'latitude': location['latitude'],
            'longitude': location['longitude'],
            'timestamp': str(datetime.now())
        }
    )

    print(f'## CREATED location with id: {event_id}')

    response = {
        "statusCode": 201,
        "body": event_id
    }

    return response
