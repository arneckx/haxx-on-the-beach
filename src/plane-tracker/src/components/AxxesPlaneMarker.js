import React, {useState} from "react";
import useInterval from 'react-use/lib/useInterval';
import ReactDOM from "react-dom";
import {Marker} from "react-leaflet";
import * as L from "leaflet";
import env from "react-dotenv";
import AxxesConfetti from "./AxxesConfetti";

function AxxesPlaneMarker() {

    const axxesPlane = L.icon({
        iconUrl: './airplane.png',
        iconSize: [60, 60]
    });

    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [position, setPosition] = useState({latitude: 51.06922482043312, longitude: 4.037307850043906});
    const mallorca = {latitude: 39.571625, longitude: 2.650544}

    useInterval(() => {
        findLastLocation();
    }, 1000);

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else if (isMallorca()) {
        return (
            <div>
                <Marker icon={axxesPlane} position={[position.latitude, position.longitude]}/>
                <AxxesConfetti/>
            </div>
        );
    }
    else
    {
        return (<Marker icon={axxesPlane} position={[position.latitude, position.longitude]}/>);
    }

    function isMallorca() {
        return position.latitude === mallorca.latitude && position.longitude === mallorca.longitude;
    }

    function findLastLocation() {
        fetch(`http://localhost:4566/restapis/${env.HAXX_ON_THE_BEACH_API_ID}/api/_user_request_/planes/haxx-on-the-beach-plane/locations?limit=1`)
            .then(response => response.json())
            .then(
                (result) => {
                    setPosition(result);
                    console.log(position)
                    setIsLoaded(true);
                },
                (error) => {
                    console.log(error)
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<AxxesPlaneMarker/>, rootElement);

export default AxxesPlaneMarker;
