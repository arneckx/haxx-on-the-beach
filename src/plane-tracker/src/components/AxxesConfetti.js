import React from 'react';
import useWindowSize from 'react-use/lib/useWindowSize';
import Confetti from 'confetti-react';

function AxxesConfetti() {
    const {width, height} = useWindowSize();

    return <div className="confetti-overlay">
        <Confetti width={width} height={height} />
    </div>;
}

export default AxxesConfetti;
