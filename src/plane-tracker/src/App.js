import './App.css';
import React, {useRef} from 'react';
import {Map, TileLayer} from 'react-leaflet';
import 'leaflet-fullscreen/dist/Leaflet.fullscreen.js';
import 'leaflet/dist/leaflet.css';
import 'leaflet-fullscreen/dist/leaflet.fullscreen.css';
import AxxesPlaneMarker from './components/AxxesPlaneMarker'

const defaultCenter = [46.62934018673026, 4.277647127325689];
const defaultZoom = 5;

function App() {
    const mapRef = useRef();

    return (
        <div className="App">
            <Map ref={mapRef} fullscreenControl={true} center={defaultCenter} zoom={defaultZoom}>
                <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                           attribution="&copy; <a href=&quot;https://www.openstreetmap.org/copyright&quot;>OpenStreetMap</a> contributors"
                />
                <AxxesPlaneMarker/>
            </Map>
        </div>
    );
}

export default App;
